exports.insertRequest = `
    INSERT INTO requests (patient_id, cnt) VALUES ($1, $2);
`;

exports.resetRequest = `
    UPDATE requests SET fulfilled = false, notified = false, cnt = $2 WHERE patient_id = $1;`;

exports.requestExists = `
    SELECT
        *
    FROM requests
    WHERE patient_id = $1
    AND fulfilled = false
    LIMIT 1;
`
exports.patientsWithRequests = `
    SELECT 
        count(tab_id) as tabs_made,
        cnt as tabs_needed
        fulfilled,
        notified
    FROM tabs t
    LEFT JOIN requests r
    ON t.patient_id = r.patient_id
    WHERE patient_id = $1
    GROUP BY patient_id, cnt, fulfilled, notified;`;

exports.requestInProgress = `
    SELECT 
        status 
    FROM maker 
    WHERE patient_id = $1;`;

exports.makerUpdate = `
    UPDATE maker SET status = $2 WHERE patient_id = $1;`;

exports.requestComplete = `
    UPDATE requests SET fulfilled = true WHERE patient_id = $1;`;
