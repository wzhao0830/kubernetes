const express = require("express");
const router = express.Router();
const { Pool } = require("pg");
const { requestExists, insertRequest } = require("../queries");

const pool = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_PORT,
});

/* GET users listing. */
router.get("/:id/:amount", async function (req, res, next) {
  const { id, amount } = req.params;

  try {
    if (isNaN(amount) || amount < 1 || amount > 30) throw 403;
    const result = await pool.query(requestExists, [id]);
    if (result.rowCount === 0) {
      const added = await pool.query(insertRequest, [id, amount]);
      if (added.rowCount === 0) throw 403;
      res.render("users", { title: "Request Added", id, amount });
      return;
    }
    res.render("usersExists", { title: "Request Failed", id });
  } catch (err) {
    console.log(err);
    res.render("usersError", { title: "Request Failed", id });
  }
});

module.exports = router;
