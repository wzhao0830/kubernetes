# WORK SUMMARY #

This details the problem and the approach to solve it.

### The Problem ###

* Micro-service: Publisher
    - "for a given patient": patient_id
    - "publishing approximatively every second the number": stream number per second

* Micro-service: Worker
    - "represented as a string uid": tab_id
    - "producing the right number of medecine tabs requested": ingest the "right number"
    - "production of each tab takes 2 seconds": create a new tab_id every 2 second

* Micro-service: Messenger
    - "deliver medecine by batch of tabs matching the patient request": Send completion signal when request fulfilled

### Infastructure ###

* Images
    - Database: Postgres
        > Query: "CREATE TABLE tabs ( tab_id SERIAL PRIMARY KEY, patient_id INTEGER NOT NULL);
        > Query: "CREATE TABLE maker ( patient_id INTEGER NOT NULL, status BOOLEAN);
        > Query: "CREATE TABLE requests ( request_id SERIAL PRIMARY KEY, patient_id INTEGER NOT NULL, cnt INTEGER NOT NULL, fulfilled BOOLEAN, notified BOOLEAN);
    - Publisher 
        > Query every second
        > Query: "SELECT count(tab_id) as tabs_made FROM tabs WHERE patient_id = {target_patient_id} GROUP BY patient_id"
        > Query: "SELECT cnt FROM requests WHERE patient_id = {target_patient_id}"
        > Query: "SELECT status FROM maker WHERE patient_id = {target_patient_id}" 
        > IF (status != (tabs_made != cnt))
        > Query: "UPDATE maker SET status = {tabs_made != cnt} WHERE patient_id = {target_patient_id}"
        > IF (tabs_made == cnt)
        > Query: "UPDATE requests SET fulfilled = true WHERE patient_id = {target_patient_id}" 
    - Worker
        > Query every 2 seconds
        > Query: "SELECT patient_id FROM maker WHERE status = true"
        > FOR EACH
        > Query: "INSERT INTO tabs (patient_id) VALUES ({patient_id})"
    - Worker 2
        > Query on-demand
        > Query:  "INSERT INTO request (patient_id, cnt) VALUES ({patient_id}, {cnt})"
    - Messenger
        > Query every second
        > Query: "Select patient_id, cnt FROM requests WHERE fulfilled = true AND notified = false"
        > Query: "UPDATE requests SET notified = true WHERE patient_id = {target_patient_id}" 
        > Print: Patient's {patient_id} request for {tab} has been filled.




### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact